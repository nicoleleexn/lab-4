package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class CartRepositoryImpl implements CartRepository{

    private HashMap<Integer, Integer> itemMap = new HashMap<>();

    @Override
    public void add(int itemId, int quantity) {
        if (itemMap.containsKey(itemId)) {
            int oldQuantity = itemMap.get(itemId);
            itemMap.replace(itemId, oldQuantity + quantity);
        } else {
            itemMap.put(itemId, quantity);
        }
    }

    @Override
    public void remove(int itemId) {
        itemMap.remove(itemId);
    }

    @Override
    public Map<Integer, Integer> getAll() {
        return itemMap;
    }
}
