package student.onlineretailer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
//@Profile("development")
@ConfigurationProperties(prefix="resources")
public class ResourcesBean {

    private String db;
    private String logs;
    private String secure;

    public String getDb() {
        return db;
    }

    public String getLogs() {
        return logs;
    }

    public String getSecure() {
        return secure;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public void setLogs(String logs) {
        this.logs = logs;
    }

    public void setSecure(String secure) {
        this.secure = secure;
    }
}
