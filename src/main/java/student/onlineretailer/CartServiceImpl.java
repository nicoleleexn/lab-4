package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CartServiceImpl implements CartService{

    @Value("#{catalog}")
    private HashMap<Integer, Item> catalog;

    @Value("${contactEmail}")
    private String contactEmail;

    @Value("${onlineRetailer.salesTaxRate}")
    private double salesTaxRate;

    @Value("${onlineRetailer.deliveryCharge.normal}")
    private double normalCharge;

    @Value("${onlineRetailer.deliveryCharge.threshold}")
    private double threshold;

    public double getSalesTaxRate() {
        return salesTaxRate;
    }

    public double getNormalCharge() {
        return normalCharge;
    }

    public double getThreshold() {
        return threshold;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    @Autowired
    private CartRepository repository;

    @Override
    public void addItemToCart(int id, int quantity) {
        if (catalog.containsKey(id)) {
            repository.add(id, quantity);
            System.out.println("Item " + id + " added to cart!");
        } else {
            System.out.println("Item " + id + " does not exist");
        }
    }

    @Override
    public void removeItemFromCart(int id) {
        repository.remove(id);
        System.out.println("Item " + id + " removed from cart!");
    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return repository.getAll();
    }

    @Override
    public double calculateCartCost() {
        Map<Integer, Integer> cart = repository.getAll();
        int cost = 0;
        for (int id: cart.keySet()) {
            int quantity = cart.get(id);
            double price = catalog.get(id).getPrice();
            cost += price * quantity;
        }
        return cost;
    }
}
