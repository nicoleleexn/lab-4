package student.onlineretailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(Application.class, args);

		CartServiceImpl cartService = context.getBean(CartServiceImpl.class);

		cartService.addItemToCart(1,2);
		cartService.addItemToCart(2,1);
		cartService.addItemToCart(3,1);
		cartService.removeItemFromCart(1);

		System.out.println("Total cost: " + cartService.calculateCartCost());
		System.out.println("Contact email: " + cartService.getContactEmail());
		System.out.println("Sales tax rate: " + cartService.getSalesTaxRate());
		System.out.println("Normal delivery charge: " + cartService.getNormalCharge());
		System.out.println("Threshold delivery charge: " + cartService.getThreshold());

		ResourcesBean resourcesBean = context.getBean(ResourcesBean.class);
		System.out.println("db: " + resourcesBean.getDb());
		System.out.println("logs: " + resourcesBean.getLogs());
		System.out.println("secure: " + resourcesBean.getSecure());
	}

	@Bean
	Map<Integer, Item> catalog() {
		HashMap<Integer, Item> catalog = new HashMap<>();
		catalog.put(1, new Item(1, "item1", 10));
		catalog.put(2, new Item(2, "item2", 20));
		catalog.put(3, new Item(2, "item3", 30));
		return catalog;
	}
}
